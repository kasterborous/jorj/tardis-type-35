local PART={}
PART.ID = "door_shelf"
PART.Name = "door_shelf"
PART.Model = "models/type35/door_shelf.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1

PART.Sound = "jorj/type35/shelf_door.wav"
PART.SoundPos = Vector(369.234, 243.55, 105.69)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
	end
end
TARDIS:AddPart(PART,e)