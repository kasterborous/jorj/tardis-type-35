local PART={}
PART.ID = "button3_1"
PART.Name = "button3_1"
PART.Model = "models/type35/button3_1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Sound = "jorj/type35/button3.wav"

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
end
if SERVER then
	function PART:Initialize()
		self:SetColor(Color(255,255,255,255))
	end
   
	function PART:Use(activator)
		self:EmitSound("tardis/capaldi/lever.wav")
		local ext=self.exterior
		if ext:GetData("vortex") then ext:Mat() else ext:Demat() end
	end
end

TARDIS:AddPart(PART,e)