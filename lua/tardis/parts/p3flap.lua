local PART={}
PART.ID = "p3flap"
PART.Name = "p3flap"
PART.Model = "models/type35/p3flap.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.3
PART.SoundOff = "jorj/type35/p3flap_close.wav"
PART.SoundOn = "jorj/type35/p3flap_open.wav"

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
end

TARDIS:AddPart(PART,e)