local PART={}
PART.ID = "shelves_left_door"
PART.Name = "shelves_left_door"
PART.Model = "models/type35/shelves_left_door.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.2

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
end

TARDIS:AddPart(PART,e)