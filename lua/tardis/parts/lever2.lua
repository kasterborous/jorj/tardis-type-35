local PART={}
PART.ID = "lever2"
PART.Name = "lever2"
PART.Model = "models/type35/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/type35/lever2.wav"

TARDIS:AddPart(PART,e)