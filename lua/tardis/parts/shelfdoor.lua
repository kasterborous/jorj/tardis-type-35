local PART={}
PART.ID = "shelfdoor"
PART.Name = "shelfdoor"
PART.Model = "models/type35/shelfdoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.3
PART.SoundOff = "jorj/type35/p3flap_close.wav"
PART.SoundOn = "jorj/type35/p3flap_open.wav"

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
	end
end
TARDIS:AddPart(PART,e)