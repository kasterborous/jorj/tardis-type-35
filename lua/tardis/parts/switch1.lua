local PART={}
PART.ID = "switch1"
PART.Name = "switch1"
PART.Model = "models/type35/switch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 3

PART.Sound = "jorj/type35/switch1.wav"

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
end

TARDIS:AddPart(PART,e)