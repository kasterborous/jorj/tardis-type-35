local PART={}
PART.ID = "console"
PART.Name = "console"
PART.Model = "models/type35/console.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
    function PART:Draw()
    self:DrawModel()
    end

    function PART:Think()
        local exterior=self.exterior
            local interior=self.interior

            if interior:GetNWBool("cloister",true) then
                if (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
                    self:SetSubMaterial(1,"fedora/jorj/rewrites/warmaster/interior/flight")
                else
                    self:SetSubMaterial(1,"fedora/jorj/rewrites/warmaster/interior/lights")
                end
            end

        if self:GetNWBool("poweron",false) then
                    self:SetSubMaterial(1,"fedora/jorj/rewrites/warmaster/interior/floor")
            end
    end
end

TARDIS:AddPart(PART,e)