local PART={}
PART.ID = "throttle1"
PART.Name = "throttle1"
PART.Model = "models/type35/throttle1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/type35/throttle1.wav"

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
end

TARDIS:AddPart(PART,e)