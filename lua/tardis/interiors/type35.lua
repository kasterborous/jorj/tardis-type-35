local T={}
T.Base="base"
T.Name ="Type 35 TARDIS"
T.ID="type35"

T.Versions = {
	randomize = false,
	allow_custom = true,
	randomize_custom = false,

	main = {
		id = "type35",
	},
		other = {
		{
			name = "Exterior 2",
			id = "type35ext2",
			},
			{
			 name = "TT Capsule",
			id = "type35capsule",
			},
			{
				name = "Jodie Exterior",
			   id = "type35jodieexterior",
			   },
		}
	}
T.Interior={
	Model="models/type35/interior.mdl",
	Portal={
		pos=Vector(0,0,50),
		ang=Angle(0,0,0),
		width=200,
		height=450
	},
	Fallback={
		pos=Vector(41,0.824,16),
		ang=Angle(0,0,0)
	},
	Sounds = {
		Teleport = {
			demat_fail = "jorj/type35/handbrake_takeoff_int.wav",
			fullflight = "jorj/type35/full.wav"

		},
		Power = {
			On = "jorj/type35/poweron.wav", -- Power On
			Off = "jorj/type35/powerdown.wav" -- Power Off
		},
		Door={
			enabled=true,
			open="jorj/type35/dooropen_int.wav",
			close="jorj/type35/doorclose_int.wav"
		},
		FlightLoop="jorj/type35/flight_int.wav",
	},
	ExitDistance=4000,
	IdleSound={
		{
			path="jorj/type35/type35hum.wav",
			volume=1
		}
	},
	Light={
	      color=Color(212,130,49),
		  warncolor=Color(255,0,0),
	      pos=Vector(368,-17,150),
	      brightness= 4.5
		},
		Parts={
				console={
					model="models/type35/panel3.mdl",
					pos=Vector(0,0,0)
			},
			walkway=true,
			rails1=true,
			rails2=true,
			whittakerparts=true,
			drawers=true,
			armchair=true,
			sofa=true,
			tables=true,
			drawersopen={
				ang=Angle(0,0,0),
				pos=Vector(17.817,92.261,4.95)
			},
			shelves_left=true,
			shelves_right=true,
			rug=true,
			rails3=true,
			table1=true,
			console=true,
			urmum=true,
			siderooms=true,
			switch1={
				ang=Angle(0,0,0),
				pos=Vector(554.984,-1.2167,47.193)
			},
			shelfdoorportal={
				ang=Angle(0,0,0),
				pos=Vector(407.05,-501.74,106.26)
			},
			shelfdoorl={
				ang=Angle(0,0,0),
				pos=Vector(407.05,-243.14,105.26)
			},
			shelfdoor1={
				ang=Angle(0,0,0),
				pos=Vector(445.24,-356.3,150.04)
			},
			shelfdoor2={
				ang=Angle(0,0,0),
				pos=Vector(323.63,-403,150.44)
			},
			shelfdoor3={
				ang=Angle(0,0,0),
				pos=Vector(451.3,404.74, 148.73)
			},
			shelfdoor4={
				ang=Angle(0,0,0),
				pos=Vector(331.3,357.34, 148.73)
			},
			switch3={
				ang=Angle(0,0,0),
				pos=Vector(545.984,-9.6308,50.621)
			},
			switch3_1={
				ang=Angle(0,0,0),
				pos=Vector(555.63,-16.05,46.798)
			},
			switch3_2={
				ang=Angle(0,0,0),
				pos=Vector(555.63,-14.572,46.798)
			},
			switch3_3={
				ang=Angle(0,0,0),
				pos=Vector(554.22,-14.572,47.247)
			},
			switch4={
				ang=Angle(0,0,0),
				pos=Vector(545.984,-7.6308,50.621)
			},
			switch5={
				ang=Angle(0,0,0),
				pos=Vector(545.984,-5.6308,50.621)
			},
			switch6={
				ang=Angle(0,0,0),
				pos=Vector(545.984,-3.1308,50.621)
			},
			switch4_1={
				ang=Angle(0,0,0),
				pos=Vector(549.5,-1.2158,49.169)
			},
			switch4_2={
				ang=Angle(0,0,0),
				pos=Vector(549.5,1.3143,49.169)
			},
			switch4_3={
				ang=Angle(0,0,0),
				pos=Vector(551.95,1.0569,48.277)
			},
			switch4_4={
				ang=Angle(0,0,0),
				pos=Vector(552,-1.2158,48.5)
			},
			switch5_1={
				ang=Angle(0,0,0),
				pos=Vector(553.26,5.8477,47.718)
			},
			switch6_1={
				ang=Angle(0,0,0),
				pos=Vector(549.35,9.6069,50.049)
			},
			switch6_2={
				ang=Angle(0,0,0),
				pos=Vector(558.06,-15.422,47.021)
			},
			slider1={
				ang=Angle(0,0,0),
				pos=Vector(542.1,25.505,48.4)
			},
			slider2={
				ang=Angle(0,0,0),
				pos=Vector(544.89,26.986,47.276)
			},
			slider3={
				ang=Angle(0,0,0),
				pos=Vector(542.37,18.977,50.266)
			},
			slider4={
				ang=Angle(0,0,0),
				pos=Vector(544.71,19.777,49.517)
			},
			desk1=true,
			keyboardandmouse=true,
			monitor=true,
			strutfour=true,
			type35_panel1stuff=true,
			glasswindow=true,
			phone=true,
			panel5stuff=true,
			consolerails=true,
			walldivider=true,
			destination1=true,
			strut1=true,
			telepathic=true,
			understairs=true,
			keypad=true,
			button3=true,
			panel4stuff=true,
			button3_1=true,
			int_box=true,
			button3_2=true,
			strut2={
				ang=Angle(0,0,0),
				pos=Vector(215,0,0)
			},
			p3flap={
				ang=Angle(0,0,0),
				pos=Vector(508.65,-18.471,52.183)
			},
			strut3={
				ang=Angle(0,0,0),
				pos=Vector(417,0,0)
			},
			strutfour={
				ang=Angle(0,0,0),
				pos=Vector(610,0,0)
			},
			stairs=true,
			button1={
				ang=Angle(0,0,0),
				pos=Vector(525.89,-39.367,46.967)
			},
			button1_1={
				ang=Angle(0,0,0),
				pos=Vector(532.14,-35.788,47.037)
			},
			button1_2={
				ang=Angle(0,0,0),
				pos=Vector(538.57,-32.088,47.344)
			},
			button1_3={
				ang=Angle(0,0,0),
				pos=Vector(544.66,-28.74,47.337)
			},
			button1_4={
				ang=Angle(0,0,0),
				pos=Vector(550.66,-25.18,47.)
			},
			button2={
				ang=Angle(0,0,0),
				pos=Vector(542.88,-21.802,50.34)
			},
			button2_1={
				ang=Angle(0,0,0),
				pos=Vector(527.26,-30.353,50.516)
			},
			panel2stuff={
				ang=Angle(0,0,0),
				pos=Vector(536.28,-29.958,47.718)
			},
			switch7={
				ang=Angle(0,0,0),
				pos=Vector(540.47,-18.968,50.913)
			},
			switch7_1={
				ang=Angle(0,0,0),
				pos=Vector(540.13,-18.303,51.216)
			},
			switch7_2={
				ang=Angle(0,0,0),
				pos=Vector(539.49,-17.587,51.304)
			},
			switch7_3={
				ang=Angle(0,0,0),
				pos=Vector(538.9,-16.716,51.614)
			},
			switch7_4={
				ang=Angle(0,0,0),
				pos=Vector(538.47,-16.058,51.934)
			},
			switch8={
				ang=Angle(0,0,0),
				pos=Vector(524.04,-22.64,52.341)
			},
			lever2={
				ang=Angle(0,0,0),
				pos=Vector(482.24,12.601,50.038)
			},
			lever3={
				ang=Angle(0,0,0),
				pos=Vector(484.68,-1.2815,46.531)
			},
			lever4={
				ang=Angle(0,0,0),
				pos=Vector(487.67,-7.8764,47.255)
			},
			lever5={
				ang=Angle(0,0,0),
				pos=Vector(509.09,18.103,52.297)
			},
			throttle1={
				ang=Angle(0,0,0),
				pos=Vector(537.4,28.216,47.422)
			},
			throttle2={
				ang=Angle(0,0,0),
				pos=Vector(533.15,30.651,47.422)
			},
			shelfdoor={
				ang=Angle(0,0,0),
				pos=Vector(369.22,243.39,105.26)
			},
			door = {
				model = "models/type35/int_door.mdl",
				posoffset = Vector(0,0,-50),
				angoffset = Angle(0, 0, 0),
				AnimateSpeed = 8
			},
					
		
		},
		PartTips = {
			lever2 = { pos = Vector(482.24,12.601,50.038), text = "Door Control", },
			keypad = { pos = Vector(486.219, 4.641, 46.641), text = "Co-ordinates", },
			lever3 = { pos = Vector(484.68,-1.2815,46.531), text = "Flight", },
			lever5 = { pos = Vector(512.005, 17.667, 52.108), text = "Power", },
			telepathic = { pos = Vector(507.401, 33.872, 45.799), text = "Manual Flight Control", },
			throttle2 = { pos = Vector(533.15,30.651,47.422), text = "Handbrake", },
			throttle1 = { pos = Vector(537.4,28.216,47.422), text = "Throttle", },
			destination1 = { pos = Vector(505.501, 27.986, 47.286), text = "Manual Destination Selection", },
			button1 = { pos = Vector(525.011, -39.793, 47.167), text = "Random Destination", },
			slider4 = { pos = Vector(547.837, 25.5, 47.051), text = "Vortex Flight", },
			switch8 = { pos = Vector(524.022, -22.977, 52.293), text = "Locking Down Mechanism", },
			switch1 = { pos = Vector(554.532, -1.268, 46.407), text = "Door Lock", },
			switch6_2 = { pos = Vector(558.431, -15.491, 46.799), text = "Isomorphic Security", },
			switch4_2 = { pos = Vector(549.673, 1.295, 49.063), text = "Self Repair", },
			switch4 = { pos = Vector(545.579, -7.362, 50.546), text = "Anti Gravs", },
			switch5_1 = { pos = Vector(553.618, 5.959, 48.042), text = "Engine Release", },
		},
		Controls = {
			lever5 		= "power",
			lever2 = "door",
			lever3 = "flight",
			keypad = "coordinates",
			telepathic = "thirdperson",
			throttle1 = "teleport",
			throttle2 = "handbrake",
			destination1 = "destination",
			button1 = "random_coords",
			slider4 = "vortex_flight",
			switch8 = "physlock",
			switch1 = "doorlock",
			table1 = "sonic_dispenser",
			switch6_2 = "isomorphic",
			switch4_2 = "repair",
			switch4 = "float",
			switch5_1 = "engine_release"
		},
		ScreensEnabled = true,
		Screens = {
			{
				pos = Vector(499, -17.8, 49.6),
				ang = Angle(0, -25, 15),
				width = 270,
				height = 175,
				visgui_rows = 4,
				power_off_black = false
			}
	
		},
		CustomPortals = {
			intportal = {
				entry = {
					pos = Vector(387, 508.0, 151.587),
					ang = Angle(0, -90, 0),
					fallback = Vector(383.245, -500.306, 108.87),
					width = 60,
					height = 100,
					link = "shelfdoorportal"
				},
				exit = {
					pos = Vector(378.265, -500.129, 151.587),
					ang = Angle(0, 90, 0),
					fallback = Vector(393.232, 500.928, 109.781),
					width = 60, height = 100,
					link = "shelfdoorportal"
				}
			},
		},
	}
	

T.Exterior={
	Model="models/type35/exteriorbox_ext.mdl",
	Mass=5000,
	Portal={
		pos=Vector(35,0,50),
		ang=Angle(0,0,0),
		width=60,
		height=100,
	},
	Parts={
	door={
		model="models/type35/ext_door.mdl",posoffset=Vector(-3,26.5,-48.4),angoffset=Angle(0,0,0), AnimateSpeed = 0.1
	},
	vortex = {
		model="models/molda/tuat/vortex/tuatvortex.mdl",
		pos=Vector(0,0,50),
		ang=Angle(0,0,0),
		scale=10
	}
},
	Fallback={
		pos=Vector(50,0,0),
		ang=Angle(0,0,0)
	},
	Light={
		color=Color(255,255,260),
		pos=Vector(0,0,114),
		brightness=3
	},
	Sounds={
		Teleport={
			demat="jorj/type35/demat.wav",
			mat="jorj/type35/mat.wav",
			demat_fail = "jorj/type35/handbrake_takeoff_int.wav",
			fullflight = "jorj/type35/full.wav"
		},
		Lock="doctorwho1200/hellbent/lock.wav",
		Door={
			enabled=true,
			open="jorj/type35/dooropen_ext.wav",
			close="jorj/type35/doorclose_ext.wav"
		},
		FlightLoop="jorj/type35/flight.wav"
	},
	Teleport={
		SequenceSpeed=0.77,
		SequenceSpeedFast=0.935,
		DematSequence={
			175,
			230,
			100,
			150,
			50,
			100,
			0
		},
		MatSequence={
			100,
			50,
			150,
			100,
			200,
			150,
			255
		}
	}
	
}
T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/jorj/type35/",
		{ "type35_panel1stuff", 0, "type35_animated" },
		{ "panel2stuff", 0, "type35_animated" },
		{ "panel5stuff", 0, "type35_animated" },
		{ "lever5", 0, "type35_animated" },
		{ "int_box", 2, "type35_windows" },
		{ "int_box", 8, "type35_windows2" },
		{ "door", 0, "type35_windows"},
		{ "door", 9, "type35_windows2"},
	},
	["flight"] = {
		prefix = "models/jorj/type35/",
		{ "int_box", 2, "type35_windowvortex" },
		{ "int_box", 8, "type35_windowvortex" },
		{ "door", 0, "type35_windowvortex"},
		{ "door", 9, "type35_windowvortex"},
	},
	["off"] = {
		prefix = "models/jorj/type35/",
		{ "type35_panel1stuff", 0, "black" },
		{ "panel2stuff", 0, "black" },
		{ "panel5stuff", 0, "black" },
		{ "lever5", 0, "black" },
	},
}
local TEXTURE_UPDATE_DATA_IDS = {
	["power-state"] = true,
	["health-warning"] = true,
	["teleport"] = true,
	["vortex"] = true,
	["flight"] = true,
}

T.CustomHooks = {
	travel_textures = {
		exthooks = {
			["DataChanged"] = true,
		},
		func = function(ext, int, data_id, data_value)
			if not TEXTURE_UPDATE_DATA_IDS[data_id] then return end
			local flight = ext:GetData("flight")
			local teleport = ext:GetData("teleport")
			local vortex = ext:GetData("vortex")
			local power = ext:GetData("power-state")
			local warning = int:GetData("health-warning", false)

			if not power then
				int:ApplyTextureSet("off")
			else
				if teleport or vortex then
					int:ApplyTextureSet("flight")
				else
					int:ApplyTextureSet("normal")
				end
			end
		end
	}
}




TARDIS:AddInterior(T)